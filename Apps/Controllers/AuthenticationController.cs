﻿
using DataLibraly.DataAccess;
using Logic.IServices;
using Logic.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;

namespace Apps.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly IAuthenticate authenticate;
        public AuthenticationController()
        {
            authenticate = new Authenticate();
        }

        public IActionResult Register()
        {
            if (HttpContext.Session.GetString("UserId") == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Register(Member member)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    authenticate.Register(member);
                    return RedirectToAction("Login", "Authentication");
                }
                catch (Exception)
                {

                    ViewBag.RegisterMessage = "Email exited";
                    return View();
                }
            }
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(string email, string password)
        {
            if (ModelState.IsValid)
            {
                var user = authenticate.Login(email, password);
                if (user != null)
                {
                    HttpContext.Session.SetString("FullName", user.Email);
                    HttpContext.Session.SetInt32("UserId", user.MemberId);

                    return RedirectToAction("Index", "Members", new { area = "" });
                }
                else
                {
                    ViewBag.LoginError = "Invalid email or password";
                    return View();
                }
            }
            return View();
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home", new { area = "" });
        }
    }
}


