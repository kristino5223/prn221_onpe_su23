﻿
using DataLibraly.DataAccess;
using Logic.IServices;
using Logic.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Apps.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IProductService productService;
        public ProductsController() => productService = new ProductService();
        // GET: ProductController
        public IActionResult Index()
        {
            CategoryService categoryRepository = new CategoryService();
            var categoryList = categoryRepository.GetAllCategories();
            ViewBag.Categories = categoryList;
            var productList = productService.GetProducts();
            return View(productList);
        }

        // GET: ProductController/Details/5
        public IActionResult Details(int? id)
        {
           
            CategoryService categoryRepository = new CategoryService();
            var categoryList = categoryRepository.GetAllCategories();
            ViewBag.Categories = categoryList;
            if (id == null) return NotFound();
            var product = productService.GetProduct(id.Value);
            if (product == null) return NotFound();
            return View(product);
        }

        // GET: ProductController/Create
        public IActionResult Create()
        {
            CategoryService categoryRepo = new CategoryService();
            var CateList = categoryRepo.GetAllCategories();
            ViewBag.Cates = CateList;
            return View();
        }

        // POST: ProductController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Product p)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    productService.AddProduct(p);
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }

        // GET: ProductController/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Product m = productService.GetProduct(id.Value);
            if (m == null)
            {
                return NotFound();
            }
            CategoryService categoryRepo = new CategoryService();
            var CateList = categoryRepo.GetAllCategories();
            ViewBag.Cates = CateList;
            return View(m);
        }

        // POST: ProductController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, Product p)
        {
            try
            {
                if (id != p.ProductId)
                {
                    return NotFound();
                }
                if (ModelState.IsValid)
                {
                    productService.UpdateProduct(p);
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                return View();
            }
        }

        // GET: ProductController/Delete/5
        public IActionResult Delete(int? id)
        {
            CategoryService categoryRepository = new CategoryService();
            var categoryList = categoryRepository.GetAllCategories();
            ViewBag.Categories = categoryList;
            if (id == null)
            {
                return NotFound();
            }
            var p = productService.GetProduct(id.Value);
            if (p == null)
            {
                return NotFound();
            }
            return View(p);
        }

        // POST: ProductController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                productService.DeleteProduct(id);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }
    }
}
