﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DataLibraly.DataAccess;
using Microsoft.AspNetCore.Http;
using Logic.IServices;
using Logic.Services;

namespace Apps.Controllers
{
    public class OrdersController : Controller
    {
        private readonly IOrderService orderService;
        public OrdersController()
        {
            orderService = new OrderService();
        }
        // GET: OrderController
        public ActionResult Index()
        {
            var orderList = orderService.GetOrders();
            return View(orderList);
        }

        // GET: OrderController/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return NotFound();
            var order = orderService.GetOrder(id.Value);
            if (order == null) return NotFound();
            return View(order);
        }

        // GET: OrderController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OrderController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Order o)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DateTime currentDate = DateTime.Now;
                    o.OrderDate = currentDate;
                    o.MemberId = HttpContext.Session.GetInt32("UserId").Value;
                    orderService.AddOrder(o);
                }

                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                return View();
            }
        }

        // GET: OrderController/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return NotFound();
            var or = orderService.GetOrder(id.Value);
            if (or == null) return NotFound();

            return View(or);
        }

        // POST: OrderController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Order o)
        {
            try
            {
                if (id != o.OrderId)
                {
                    return NotFound();
                }
                if (ModelState.IsValid)
                {
                    orderService.UpdateOrder(o);
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                return View();
            }
        }
        // GET: ProductController/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var p = orderService.GetOrder(id.Value);
            if (p == null)
            {
                return NotFound();
            }
            return View(p);
        }

        // POST: ProductController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                orderService.DeleteOrder(id);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }
    }
}
