﻿
using Logic.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace eStore.Controllers
{
    public class StatisticalController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Statistical(DateTime StDate, DateTime EdDate)
        {
            string _stDate = StDate.ToString();
            string _edDate = EdDate.ToString();
            OrderService orderService = new OrderService();
            var orderList = orderService.GetOrders();
            HttpContext.Session.SetString("StDate", _stDate);
            HttpContext.Session.SetString("EdDate", _edDate);
            return View(orderList);
        }
    }
}
