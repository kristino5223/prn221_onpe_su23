﻿
using DataLibraly.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.IServices
{
    public interface IOrderService
    {
        IEnumerable<Order> GetOrders();

        void DeleteOrder(int idO);

        Order GetOrder(int id);

        void UpdateOrder(Order o);

        void AddOrder(Order o);
    }
}
