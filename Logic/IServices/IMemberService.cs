﻿using DataLibraly.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Logic.IServices
{
    public interface IMemberService
    {
        IEnumerable<Member> GetMembers();
        Member GetMemberByID(int MemberId);
        void InsertMember(Member member);
        void UpdateMember(Member member);
        void DeleteMember(int MemberId);
    }
}
