﻿using DataLibraly.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.IServices
{
    public interface IAuthenticate
    {
        Member Login(string username, string password);
        void Register(Member member);
    }
}
