﻿
using DataLibraly.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.IServices
{
    public interface ICategoryService
    {
        IEnumerable<Category> GetAllCategories();
        Category GetCategoryByID(int CategoryId);
        void Insert(Category category);
        void Update(Category category);
        void Delete(int CategoryId);
    }
}
