﻿
using DataLibraly.DataAccess;
using Logic.DAO;
using Logic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Services
{
    public class CategoryService : ICategoryService
    {
        public void Delete(int CategoryId) => CategoryDAO.Instance.Delete(CategoryId);

        public IEnumerable<Category> GetAllCategories() => CategoryDAO.Instance.GetListCategories();
        public Category GetCategoryByID(int CategoryId) => CategoryDAO.Instance.GetCategoryByID(CategoryId);

        public void Insert(Category category) => CategoryDAO.Instance.Insert(category);

        public void Update(Category category) => CategoryDAO.Instance.Update(category);
    }
}
