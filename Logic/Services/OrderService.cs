﻿
using DataLibraly.DataAccess;
using Logic.DAO;
using Logic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Services
{
    public class OrderService : IOrderService
    {
        public void AddOrder(Order o) => OrderDAO.Instance.AddOrder(o);


        public void DeleteOrder(int idO) => OrderDAO.Instance.Delete(idO);


        public Order GetOrder(int id) => OrderDAO.Instance.GetOrderByID(id);


        public IEnumerable<Order> GetOrders() => OrderDAO.Instance.GetOrders();

        public void UpdateOrder(Order o) => OrderDAO.Instance.Modify(o);
    }
}
