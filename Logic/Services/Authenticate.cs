﻿using DataLibraly.DataAccess;
using Logic.DAO;
using Logic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Services
{
    public class Authenticate : IAuthenticate
    {
        public Member Login(string username, string password) => AuthenticateDAO.Instance.Login(username, password);

        public void Register(Member member) => AuthenticateDAO.Instance.Register(member);
    }
}
