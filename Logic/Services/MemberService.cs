﻿using DataLibraly.DataAccess;
using Logic.DAO;
using Logic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Logic.Services
{
    public class MemberService : IMemberService
    {
        public void DeleteMember(int MemberId) => MemberDAO.Instance.DeleteMember(MemberId);

        public Member GetMemberByID(int MemberId) => MemberDAO.Instance.GetMemberByID(MemberId);


        public IEnumerable<Member> GetMembers() => MemberDAO.Instance.GetMemberList();


        public void InsertMember(Member member) => MemberDAO.Instance.InsertMember(member);



        public void UpdateMember(Member member) => MemberDAO.Instance.UpdateMember(member);


    }
}
