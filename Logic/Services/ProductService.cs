﻿
using DataLibraly.DataAccess;
using Logic.DAO;
using Logic.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Services
{
    public class ProductService : IProductService
    {
        public void AddProduct(Product p) => ProductDAO.Instance.AddProduct(p);


        public void DeleteProduct(int id) => ProductDAO.Instance.delete(id);


        public Product GetProduct(int id) => ProductDAO.Instance.GetProductByID(id);


        public IEnumerable<Product> GetProducts() => ProductDAO.Instance.GetProducts();


        public void UpdateProduct(Product p) => ProductDAO.Instance.modify(p);

    }
}
